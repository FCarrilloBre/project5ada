# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 19:12:18 2019

@author: fcarr_000
"""

import numpy as np
import math
from scipy.io import wavfile
import matplotlib.pyplot as plt
from drawnow import drawnow
import scipy.signal as sps
import librosa
import pygame
import scipy

#Importar cancion
signal,fs_rate=librosa.load("Beatles.wav")

#Definir FFT
def FFT(nu):
    n = len(nu)

    if n == 1: return nu

    izq= [num for index, num in enumerate(nu) if index % 2 == 0]
    der = [num for index, num in enumerate(nu) if index % 2 == 1]

    izq_transf = FFT(izq)
    der_transf = FFT(der)

    inv_root = math.e ** (2 * math.pi * 1j / n)
    root = 1

    res = [0] * n
    for index in range(0, int(n/2)):
        res[index] = izq_transf[index] + root * der_transf[index]
        res[int(index + n/2)] = izq_transf[index] - root * der_transf[index]
        root = root * inv_root
    return res
#

def IFFT(nu):
    n = len(nu)

    if n == 1: return nu

    izq= [num for index, num in enumerate(nu) if index % 2 == 0]
    der = [num for index, num in enumerate(nu) if index % 2 == 1]

    izq_transf = IFFT(izq)
    der_transf = IFFT(der)

    inv_root = math.e ** -(2 * math.pi * 1j / n)
    root = 1

    res = [0] * n
    for index in range(0, int(n/2)):
        res[index] = (izq_transf[index] + root * der_transf[index])/2
        res[int(index + n/2)] = (izq_transf[index] - (root * der_transf[index]))/2
        root = root * inv_root
    return res

## Generar tiempo
Time = np.arange(signal.shape[0]) / fs_rate

# Ploter cancion
plt.figure(1)
plt.plot(Time, signal)
plt.xlabel('Time(s)')
plt.ylabel('Amplitude')

# resamplear para optimizar proceso
### se hara con 1/8 de la frecuencia original
new_rate=8192
signalresample = librosa.resample(signal,fs_rate, new_rate)

## se guarda el nuevo archivo con resample
#librosa.output.write_wav('Beatlesresample.wav', signalresample, new_rate)
wavfile.write('Beatlesresample.wav',new_rate,signalresample)

### Se plotea la cancion resampleada
Time2 = np.arange(signalresample.shape[0]) / new_rate
plt.figure(2)
plt.plot(Time2, signalresample)
plt.xlabel('Time(s)')
plt.ylabel('Amplitude')

### TRansformada de Fourier 
fft=FFT(signalresample[0:8192])
fft2=scipy.fftpack.fft(signalresample[0:8192])
FT=[]
for i in range(0,len(fft)):
    FT.append(abs(fft[i]))

w = np.linspace(0, len(signalresample[0:8192]), len(fft))
fourier_to_plot = FT[0:len(FT)//2]
w = w[0:len(fft)//2]

#### FFT en un ciclo de toda la longitud
FFTL=[]
pedazos=round(len(signalresample)/new_rate)
for i in range(0,pedazos):
    FFTL.append(FFT(signalresample[(new_rate*i):(new_rate*(i+1))]))

ifft=IFFT(fft)
ifft2=scipy.fftpack.ifft(fft2)
plt.figure(3)
plt.plot(w, fourier_to_plot)
plt.xlabel('frequency')
plt.ylabel('amplitude')
plt.show()
#
plt.figure(4)
plt.subplot(211)
plt.plot(Time2, signalresample)
plt.xlabel('Time(s)')
plt.ylabel('Amplitude')
plt.subplot(212)
plt.plot(w, fourier_to_plot)
plt.xlabel('frequency')
plt.ylabel('amplitude')
plt.show()
#
#######################3
#    
IFFTL=[]
for i in range(0,len(FFTL)):
    IFFTL.append(IFFT(FFTL[i]))
    
IFFT = []
for i in range(0,len(IFFTL)):
    IFFT.extend(IFFTL[i])

IFFT = np.array(IFFT)    
ifftres=IFFT.astype(signalresample.dtype)
wavfile.write('Beatlesrecovered.wav',new_rate,ifftres)

#input()
#plt.figure(5)
#x=[]
#y=[]
#i=0
#w = np.linspace(0, len(signalresample[0:8192]), len(fft))
#while i<=pedazos-1:
#    plt.subplot(211)
#    x.extend(Time2[(new_rate*i):(new_rate*(i+1))])
#    y.extend(signalresample[(new_rate*i):(new_rate*(i+1))])
#    plt.plot(x,y)
#    plt.xlabel('Time(s)')
#    plt.ylabel('Amplitude')
#    w = w[0:len(fft)//2]
#    fourier_to_plot = FFTL[i]
#    fourier_to_plot = fourier_to_plot[0:len(fourier_to_plot)//2]
#    plt.subplot(212)
#    plt.plot(w, fourier_to_plot)
#    plt.xlabel('frequency')
#    plt.ylabel('amplitude')
#    plt.show()
#    plt.pause(2)
#    i=i+1
#    plt.close()
#    
